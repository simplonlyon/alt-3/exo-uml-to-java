package co.simplon.alt3;

import java.time.LocalDate;
import java.util.List;

import co.simplon.alt3.exouml.Book;
import co.simplon.alt3.exouml.BookCopy;
import co.simplon.alt3.exouml.User;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        User user = new User(1, "oui", LocalDate.of(2023, 1, 1));

        Book book = new Book(1, "Livre", "non");
        book.setCopies(List.of(
            new BookCopy(1, "Villeurbanne", book),
            new BookCopy(2, "Lyon", book)
        ));

        book.borrow(user);
    }
}
