package co.simplon.alt3.exouml;

public class BookCopy {
    private Integer id;
    private String location;
    private Book book;
    private User borrower;

    public BookCopy(Integer id, String location, Book book) {
        this.id = id;
        this.location = location;
        this.book = book;
    }

    public BookCopy(Integer id, String location, Book book, User borrower) {
        this.id = id;
        this.location = location;
        this.book = book;
        this.borrower = borrower;
    }

    public boolean isAvailable() {
        return borrower == null;
    }

    public void setBorrowed(User user) {
        borrower = user;
        user.addBorrowed(this);

    }
    
    
}
