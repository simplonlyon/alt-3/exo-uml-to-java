package co.simplon.alt3.exouml;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class User {
    private Integer id;
    private String username;
    private LocalDate membershipExpire;
    private boolean membershipSuspended = false;
    private List<BookCopy> borrowed = new ArrayList<>();

    public User(Integer id, String username, LocalDate membershipExpire) {
        this.id = id;
        this.username = username;
        this.membershipExpire = membershipExpire;
    }
    
    public boolean checkMembership() {
        return !membershipSuspended && membershipExpire.isAfter(LocalDate.now());
    }

    public void addBorrowed(BookCopy copy) {
        borrowed.add(copy);
    }

}
