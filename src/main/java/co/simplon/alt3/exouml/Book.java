package co.simplon.alt3.exouml;

import java.util.ArrayList;
import java.util.List;

public class Book {
    private Integer id;
    private String title;
    private String author;
    private List<BookCopy> copies = new ArrayList<>();

    public Book(Integer id, String title, String author) {
        this.id = id;
        this.title = title;
        this.author = author;
    }

    public BookCopy checkAvailable() {
        for (BookCopy copy : copies) {
            if(copy.isAvailable()) {
                return copy;
            }
        }
        return null;
    }

    public boolean borrow(User user) {
        if(user.checkMembership()) {
            BookCopy available = checkAvailable();
            if(available != null) {
                available.setBorrowed(user);
                return true;
            }
        }

        return false;
        
    }

    public void setCopies(List<BookCopy> copies) {
        this.copies = copies;
    }

    
    
}
